<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	//public $db;
	public function __construct() {
        parent::__construct();
        $this->load->model('Utility');
		//$test = new Test();
		//$this->db = $this->Utility->db;
		$this->load->library('session');
    }
	

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('login');
	}		
	public function register()
	{
		$this->load->helper('url');
		$this->load->view('registration');
	}
	public function login()
	{
		//session_start();
			$name = $this->input->post('name');
			$password = $this->input->post('password');

			$admin = $this->Utility->get_Admin();

			$data = $this->Utility->get_user($name,$password);

			foreach ($data as $user) {
				if ($user['pseudo'] == $name && $user['mdp'] == $password) {
					foreach($admin as $administrateur){
						if($user['pseudo'] == $administrateur['pseudo'] && $user['mdp'] == $administrateur['mdp']){
							$id = $user['id'];
							
							$this->session->set_userdata('idUser',$id);
							$this->session->set_userdata('name', $name);
							$this->session->set_userdata('password', $password);
							/*$_SESSION['name'] = $name;
							$_SESSION['idUser'] = $id;
							$_SESSION['password'] = $password;*/
							$this->load->helper('url');
							redirect('http://[::1]/FrontOffice/index.php/Admin/index');
						}else{
							$id = $user['id'];
							
							$this->session->set_userdata('idUser',$id);
							$this->session->set_userdata('name', $name);
							$this->session->set_userdata('password', $password);

							$this->load->helper('url');
							redirect('http://[::1]/FrontOffice/index.php/Client/index',$id,$name,$password);
						}
					}
				}
			}

			
			$this->session->set_userdata('name', $name);
			$this->session->set_userdata('password', $password);

			$this->load->helper('url');
			redirect('http://[::1]/FrontOffice/index.php/Welcome/index');
	}


	public function Addregister()
	{
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$name = $this->input->post('name');
			$password = $this->input->post('password');

			$this->Utility->add_user($name,$password);

			$this->load->helper('url');
			$this->load->view('registration');
		}
	}
}

