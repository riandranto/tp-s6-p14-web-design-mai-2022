<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Utility');
		$this->load->library('session');
	}
	
	public function index() {
		// Initialiser la session si elle n'a pas été démarrée
		if (!$this->session->userdata('initialized')) {
			$this->session->set_userdata(array('initialized' => TRUE));
		}
		
		// Accéder aux données de session
		$idUser = $this->session->userdata('idUser');
		$nomUser = $this->session->userdata('name');
	
		// Charger les données pour la vue
		$data['publish'] = $this->Utility->getActuPublier();
	
		// Charger la vue
		$this->load->helper('url');
		$this->load->view('Frontoffice/index', array('data' => $data, 'idUser' => $idUser, 'nomUser' => $nomUser));
	}

	public function post(){
		$this->load->helper('url');
		$this->load->view('Frontoffice/post');
	}

	public function publish(){
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
	
			// Charger la bibliothèque d'upload
			$this->load->library('upload');
	
			// Configurer les paramètres de l'upload
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = 2048;
			$this->upload->initialize($config);
	
			// Valider et traiter l'upload de l'image
			if ($this->upload->do_upload('userfile')) {
				$upload_data = $this->upload->data();
				$image_path = 'uploads/'.$upload_data['file_name'];
	
				echo "L'image a été téléchargée avec succès.";
			} else {
				$error = array('error' => $this->upload->display_errors());
	
				echo "Une erreur s'est produite lors du téléchargement de l'image.";
			}
	
			// Récupérer l'identifiant et le nom de l'utilisateur depuis la session
			$idUser = $this->session->userdata('idUser');
			$nomUser = $this->session->userdata('name');
	
			// Récupérer les données du formulaire
			$titre = $this->input->post('titre');
			$propos = $this->input->post('descri');
			$type = $this->input->post('type');
	
			// Ajouter la nouvelle actualité dans la base de données
			if(isset($image_path)){
				$this->Utility->AddNewActu($idUser, $nomUser, $titre, $propos, $image_path);
			} else {
				$this->Utility->AddNewActu($idUser, $nomUser, $titre, $propos, '');
			}
	
			// Initialiser la session
			if (!$this->session->userdata('initialized')) {
				$this->session->set_userdata(array('initialized' => TRUE));
			}
	
			// Charger les actualités publiées et afficher la page d'accueil
			$data['publish'] = $this->Utility->getActuPublier();
			$idUser = $this->session->userdata('idUser');
			$nomUser = $this->session->userdata('name');
			$this->load->helper('url');
			$this->load->view('Frontoffice/index', array('data' => $data, 'idUser' => $idUser, 'nomUser' => $nomUser));
		}
	}
}