<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Utility');
		//$test = new Test();
		//$this->db = $this->Utility->db;
    }

    public function index()
    {
        $data['publish'] = $this->Utility->getActuPublier();

        $this->load->helper('url');
		$this->load->view('Backoffice/index',$data);
    }

    public function ValidateContent(){
        if ($this->input->server('REQUEST_METHOD') === 'GET') {
            $idActualite = $this->input->get('idActualite');
    
            if($this->Utility->AddValidate($idActualite)){
                $data['publish'] = $this->Utility->getActuValider();
                $this->load->helper('url');
                $this->load->view('Backoffice/publish',$data);
            } else {
                $data['Actualite'] = $this->Utility->getNouveauActu();
                $this->load->helper('url');
                $this->load->view('Backoffice/validate',$data);
            }
        }
    }

    public function validate(){
        $data['Actualite'] = $this->Utility->getNouveauActu();

        $this->load->helper('url');
        $this->load->view('Backoffice/validate',$data);
    }
    public function publishContent(){
        $this->load->helper('url');
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $idActualite = $this->input->post('idActualite');
            $date = $this->input->post('date');

            $this->Utility->AddPost($idActualite,$date);

            $data['publish'] = $this->Utility->getActuPublier();

            $this->load->view('Backoffice/index',$data);
        }
    }

    public function publish(){
        $data['publish'] = $this->Utility->getActuValider();

        $this->load->helper('url');
        $this->load->view('Backoffice/publish',$data);
    }
}