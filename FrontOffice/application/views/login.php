<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login</title>
    <link rel="stylesheet" href="<?php echo site_url('assets/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo site_url('https://fonts.googleapis.com/css?family=Montserrat:400,400i,700,700i,600,600i&amp;display=swap')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/fonts/simple-line-icons.min.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.min.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/css/vanilla-zoom.min.css')?>">
</head>

<body>
    <nav class="navbar navbar-light navbar-expand-lg fixed-top bg-white clean-navbar">
        <div class="container"><a class="navbar-brand logo" href="#">Actu</a><button data-bs-toggle="collapse" class="navbar-toggler" data-bs-target="#navcol-1"><span class="visually-hidden">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
               
            </div>
        </div>
    </nav>
    <main class="page login-page">
        <section class="clean-block clean-form dark">
            <div class="container">
                <div class="block-heading">
                    <h2 class="text-info">Log In</h2>
                </div>
                <form action="<?php echo site_url("index.php/Welcome/login"); ?>" method="post">
                    <div class="mb-3"><label class="form-label" >Name</label><input class="form-control item" type="text" name="name"></div>
                    <div class="mb-3"><label class="form-label" for="password">Password</label><input class="form-control" type="password" name="password"></div>
                    <div class="mb-3">
                        
                    </div>
                    <center><button class="btn btn-primary" type="submit">Log In</button></center>
                    <hr>
                    <center><a href="<?php echo site_url("index.php/welcome/register"); ?>">Sign Up</a></center>
                </form>
            </div>
        </section>
    </main>
    <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo site_url('https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.min.js')?>"></script>
    <script src="<?php echo site_url('assets/js/vanilla-zoom.js')?>"></script>
    <script src="<?php echo site_url('assets/js/theme.js')?>"></script>
</body>

</html>