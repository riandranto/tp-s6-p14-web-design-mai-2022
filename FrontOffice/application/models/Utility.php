<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utility extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database(); 
    }
    
    public function get_users() {
        $query = $this->db->query("SELECT * FROM users");            
           // $val=$query->result_array();
        return $query->result_array();
    }
    
    public function get_user_by_id($id) {
        $query = $this->db->query("SELECT * FROM users where id=$id");
        return $query->row();
    }
    public function get_user($nom,$mdp) {
        //$this->db->select('*');
       // $this->db->from('Users');
        //$this->db->where('pseudo', $nom);
        //$this->db->where('mdp', $mdp);
        $query = $this->db->query("SELECT * FROM users where pseudo='$nom' and mdp='$mdp'");
        return $query->result_array();
    }
    
    public function add_user($nom,$mdp) {
        $data = array(
            'pseudo' => $nom,
            'mdp' => $mdp,
        );
        $this->db->insert('users', $data);
    }
    
    public function update_user($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('users', $data);
        return true;
    }
    
    public function delete_user($id) {
        $this->db->where('id', $id);
        $this->db->delete('users');
        return true;
    }

    // Récupération AdminUser
    public function get_Admin() {
        $query = $this->db->query("SELECT * FROM administrateur");
        return $query->result_array();
    }
    public function getNouveauActu(){
        $query = $this->db->query("SELECT * FROM actualite where etat=0");
        return $query->result_array();
    }

    public function UpdateActu($id){
        $data = array(
            'etat' => 1
        );
        $this->db->where('id', $id);
        $this->db->update('actualite', $data);

        return true;
    }

    public function getActuValider(){
        $query = $this->db->query("SELECT a.* FROM actualite a, valider v where v.idactualite=a.id and a.etat=1");
        return $query->result_array();
    }
    public function getActuPublier(){
        $query = $this->db->query("SELECT a.*,p.datepublication FROM actualite a,publier p where p.idactualite=a.id");
        return $query->result_array();
    }

    public function AddPost($idActualite,$date){
        $data = array(
            'idactualite' => $idActualite,
            'datepublication' => $date
        );
        $this->db->insert('publier', $data);
    }

    public function AddValidate($idActualite){
        if($this->UpdateActu($idActualite)){
            $data = array(
                'idactualite' => $idActualite
            );
            $this->db->insert('valider', $data);
            return true;
        }
        return false;
    }

    public function AddNewActu($idUser,$nomUser,$Titre,$propos,$photo){
        $data = array(
            'idmpanoratra' => $idUser,
            'auteur' => $nomUser,
            'titre' => $Titre,
            'apropos' => $propos,
            'url' => $photo,
            'etat' => 0
        );
        $this->db->insert('actualite',$data);
    }
}