<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        //$this->load->database();
    }

    public function get_all_Users()
    {
        $query = $this->db->get('Users');
        return $query->result();
    }

    public function get_book_by_id($id)
    {
        $query = $this->db->get_where('Users', array('id' => $id));
        return $query->row();
    }

    public function create_book($data)
    {
        return $this->db->insert('Users', $data);
    }

    public function update_book($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('Users', $data);
    }

    public function delete_book($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('Users');
    }

}